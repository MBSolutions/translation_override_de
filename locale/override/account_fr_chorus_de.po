#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:account.configuration,chorus_login:account_fr_chorus."
msgid "Chorus Login"
msgstr "Chorus Login"

msgctxt "field:account.configuration,chorus_password:account_fr_chorus."
msgid "Chorus Password"
msgstr "Chorus Passwort"

msgctxt "field:account.configuration,chorus_service:account_fr_chorus."
msgid "Chorus Service"
msgstr "Chorus Dienst"

msgctxt "field:account.configuration,chorus_syntax:account_fr_chorus."
msgid "Chorus Syntax"
msgstr "Chorus Syntax"

msgctxt "field:account.credential.chorus,chorus_login:account_fr_chorus."
msgid "Login"
msgstr "Anmeldename"

msgctxt "field:account.credential.chorus,chorus_password:account_fr_chorus."
msgid "Password"
msgstr "Passwort"

msgctxt "field:account.credential.chorus,chorus_service:account_fr_chorus."
msgid "Service"
msgstr "Service"

msgctxt "field:account.credential.chorus,company:account_fr_chorus."
msgid "Company"
msgstr "Unternehmen"

msgctxt "field:account.credential.chorus,create_date:account_fr_chorus."
msgid "Create Date"
msgstr "Erstellungsdatum"

msgctxt "field:account.credential.chorus,create_uid:account_fr_chorus."
msgid "Create User"
msgstr "Erstellt durch"

msgctxt "field:account.credential.chorus,id:account_fr_chorus."
msgid "ID"
msgstr "ID"

msgctxt "field:account.credential.chorus,rec_name:account_fr_chorus."
msgid "Record Name"
msgstr "Name"

msgctxt "field:account.credential.chorus,write_date:account_fr_chorus."
msgid "Write Date"
msgstr "Zuletzt geändert"

msgctxt "field:account.credential.chorus,write_uid:account_fr_chorus."
msgid "Write User"
msgstr "Letzte Änderung durch"

msgctxt "field:account.invoice.chorus,create_date:account_fr_chorus."
msgid "Create Date"
msgstr "Erstellungsdatum"

msgctxt "field:account.invoice.chorus,create_uid:account_fr_chorus."
msgid "Create User"
msgstr "Erstellt durch"

msgctxt "field:account.invoice.chorus,date:account_fr_chorus."
msgid "Date"
msgstr "Datum"

msgctxt "field:account.invoice.chorus,id:account_fr_chorus."
msgid "ID"
msgstr "ID"

msgctxt "field:account.invoice.chorus,invoice:account_fr_chorus."
msgid "Invoice"
msgstr "Rechnung"

msgctxt "field:account.invoice.chorus,number:account_fr_chorus."
msgid "Number"
msgstr "Nummer"

msgctxt "field:account.invoice.chorus,rec_name:account_fr_chorus."
msgid "Record Name"
msgstr "Name"

msgctxt "field:account.invoice.chorus,syntax:account_fr_chorus."
msgid "Syntax"
msgstr "Syntax"

msgctxt "field:account.invoice.chorus,write_date:account_fr_chorus."
msgid "Write Date"
msgstr "Zuletzt geändert"

msgctxt "field:account.invoice.chorus,write_uid:account_fr_chorus."
msgid "Write User"
msgstr "Letzte Änderung durch"

msgctxt "field:party.party,chorus:account_fr_chorus."
msgid "Chorus Pro"
msgstr "Chorus Pro"

msgctxt "help:party.party,chorus:account_fr_chorus."
msgid "Send documents for this party through Chorus Pro"
msgstr "Dokumente dieser Partei über Chorus Pro senden"

msgctxt "model:account.credential.chorus,name:account_fr_chorus."
msgid "Account Credential Chorus"
msgstr "Kontenberechtigung Chorus"

msgctxt "model:account.invoice.chorus,name:account_fr_chorus."
msgid "Invoice Chorus"
msgstr "Chorus Rechnung"

msgctxt "model:ir.action,name:account_fr_chorus.act_invoice_chorus_form"
msgid "Chorus"
msgstr "Chorus"

msgctxt "model:ir.message,text:account_fr_chorus.msg_invoice_address_no_siret"
msgid ""
"To create a Chorus invoice for \"%(invoice)s\", you must set a SIRET on "
"address \"%(address)s\"."
msgstr ""
"Auf Adresse \"%(address)s\" muss eine SIRET gesetzt sein, damit eine Chorus "
"Rechnung für \"%(invoice)s\" erstellt werden kann."

msgctxt "model:ir.message,text:account_fr_chorus.msg_invoice_delete_sent"
msgid "You cannot delete Chorus invoice \"%(invoice)s\" because it is sent."
msgstr ""
"Die Chorus Rechnung \"%(invoice)s\" kann nicht gelöscht werden, da sie "
"bereits versendet wurde."

msgctxt "model:ir.message,text:account_fr_chorus.msg_invoice_unique"
msgid "You can create only one Chorus invoice per invoice."
msgstr "Es kann nur eine Chorus Rechnung pro Rechnung generiert werden."

msgctxt "model:ir.ui.menu,name:account_fr_chorus.menu_invoice_chorus_form"
msgid "Chorus"
msgstr "Chorus"

msgctxt "selection:account.configuration,chorus_service:account_fr_chorus."
msgid "Production"
msgstr "Produktion"

msgctxt "selection:account.configuration,chorus_service:account_fr_chorus."
msgid "Qualification"
msgstr "Qualifizierung"

msgctxt ""
"selection:account.credential.chorus,chorus_service:account_fr_chorus."
msgid "Production"
msgstr "Produktion"

msgctxt ""
"selection:account.credential.chorus,chorus_service:account_fr_chorus."
msgid "Qualification"
msgstr "Qualifizierung"

msgctxt "selection:ir.cron,method:account_fr_chorus."
msgid "Send invoices to Chorus"
msgstr "Rechnungen an Chorus senden"

msgctxt "view:account.configuration:account_fr_chorus."
msgid "Chorus"
msgstr "Chorus"
